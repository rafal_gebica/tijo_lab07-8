package pl.edu.pwsztar.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.FileDto;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Component
public interface FileService {
    List<FileDto> getMoviesSorted();
    InputStreamResource getInputStream(File file) throws IOException;
    void saveMovie(List<FileDto> fileDto, File file) throws IOException;
}
