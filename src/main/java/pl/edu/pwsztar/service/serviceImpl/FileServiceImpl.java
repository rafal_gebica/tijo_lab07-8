package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.domain.files.FileGenerateTxt;
import pl.edu.pwsztar.domain.repository.MovieRepository;
import pl.edu.pwsztar.service.FileService;

import java.io.*;
import java.util.List;

@Service
public class FileServiceImpl implements FileGenerateTxt, FileService {

    private final MovieRepository movieRepository;
    private final Converter<List<Movie>, List<FileDto>> converter;

    public FileServiceImpl(MovieRepository movieRepository,
                           Converter<List<Movie>, List<FileDto>> converter) {
        this.movieRepository = movieRepository;
        this.converter = converter;
    }

    @Override
    public List<FileDto> getMoviesSorted() {
        List<Movie> movies = movieRepository.findAll(Sort.by("year").descending());
        return converter.convert(movies);
    }

    @Override
    public InputStreamResource getInputStream(File file) throws IOException {
        InputStream inputStream = new FileInputStream(file);
        return new InputStreamResource(inputStream);
    }

    @Override
    public void saveMovie(List<FileDto> fileDto, File file) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(file);

        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));

        for(FileDto toFile : fileDto){
            bufferedWriter.write(toFile.getYear() + " " + toFile.getTitle());
            bufferedWriter.newLine();
        }

        bufferedWriter.close();
        outputStream.flush();
        outputStream.close();
    }

    @Override
    public InputStreamResource toTxt(List<FileDto> fileDto) throws IOException {
        File file=File.createTempFile("tmp", ".txt");
        saveMovie(fileDto, file);
        return getInputStream(file);
    }
}
