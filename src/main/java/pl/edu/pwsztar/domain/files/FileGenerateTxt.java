package pl.edu.pwsztar.domain.files;

import org.springframework.core.io.InputStreamResource;
import pl.edu.pwsztar.domain.dto.FileDto;

import java.io.IOException;
import java.util.List;

public interface FileGenerateTxt {
    InputStreamResource toTxt(List<FileDto> fileDto) throws IOException;
}
