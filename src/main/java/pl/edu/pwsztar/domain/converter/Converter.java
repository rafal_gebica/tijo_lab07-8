package pl.edu.pwsztar.domain.converter;

import org.springframework.stereotype.Component;

@FunctionalInterface
@Component
public interface Converter<F, T> {
    T convert(F from);
}
